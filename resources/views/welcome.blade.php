<x-layout>
    
    <header class="header-img">
        <div class="container">
            @if (session('access.revisor'))
            
                    <div class="alert alert-danger col-4 d-flex" role="alert">
                        <p>
                            {{session('access.revisor')}}
                        </p>
                            
                        <button type="button" class="btn-close btn-close ms-auto" data-bs-dismiss="alert" aria-label="Close">
                        </button>
                    </div>
                    
            @endif
            <div class="row min-vh-100 justify-content-center">
                <div class="col-12 d-flex justify-content-center align-items-center flex-column">
                    <h1 class="header-title anim-left text-center">{{ __('ui.welcome')}}</h1>
                    <a href="{{route('articles.create')}}" class="mt-5"><button class="c-button">{{ __('ui.insert_button')}}</button></a>
                    
                </div>
                <div class="col-10 anim-left">
                    <h2 class="text-center mb-4 text-white">{{ __('ui.search_title')}}</h2>
                    <form action="{{route('search.results')}}" class="d-flex" method="GET">
                        
                        <input name="q" class="form-control me-2" type="text" placeholder="Es. moto, telefono etc.." aria-label="Search">
                    
                    <a href="{{route('search.results')}}"><button class="btn btn-outline-light" type="submit">{{ __('ui.search_button')}}</button></a>
                </form>
                </div>
            </div>
        </div>
    </header>


    <div class="container mb-5">
        <div class="row ">
            <h2 class="text-center fs-1 ">{{ __('ui.last_ads')}}</h2>
            @foreach ($articles as $article)
            <div class="col-12 col-md-4 mt-4">
                <div class="card">
                    {{-- Carousel --}}
                    <div id="ad-{{$article->id}}" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            @foreach($article->images as $image)
                            <div class="carousel-item @if($loop->first) active @endif">
                                <img src="{{$image->getUrl(300, 150)}}" alt="" class="img-fluid card-img-top">
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#ad-{{$article->id}}" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#ad-{{$article->id}}" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                            @endforeach
                        </div>
                    </div>
                    
                    <div class="card-body text-white">
                        <h5 class="card-title text-truncate">Titolo: {{$article->title}}</h5>
                        @foreach ($article->category as $category)
                            <a href="{{route('category.show', compact('category'))}}" class="card-text">{{$category->name}}</a>
                        @endforeach
                        <p class="card-text text-truncate">{{$article->body}}</p>
                        <p class="card-text">€ {{$article->price}}</p>
                        <a href="{{route('articles.show',compact('article'))}}" class="btn btn-primary">Visualizza</a>
                        {{-- <a href="{{route('articles.edit',compact('article'))}}" class="btn btn-warning"><i class="fas fa-edit "></i> Modifica</a> --}}

                        {{-- <form action="{{route('articles.delete',compact('article'))}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger my-2"><i class="fas fa-trash me-2"></i>Elimina</button>
                        </form> --}}
                    </div>
                </div>

            </div>

            @endforeach
        </div>
    </div>







</x-layout>