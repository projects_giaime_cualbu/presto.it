<x-layout>

    <div class="container mt-4 mb-4 vh-100">
        <div class="row">
            <div class="col-12">
                <h1>Articolo {{$article->title}}</h1>
            </div>
        </div>

        <!-- Success Message -->
        @if (session('message'))
        <div class="alert alert-success py-2 shadow my-4">
            <p>{{session('message')}}</p>
        </div>
        @endif



        <div class="card text-white">
            {{-- Carousel --}}
            <div id="ad-{{$article->id}}" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    @foreach($article->images as $image)
                    <div class="carousel-item @if($loop->first) active @endif">
                        <img src="{{$image->getUrl(300, 150)}}" alt="" class="img-fluid card-img-top">
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#ad-{{$article->id}}" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#ad-{{$article->id}}" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                    @endforeach
                </div>
            </div>
            <div class="card-body">
                <h5 class="card-title">Titolo: {{$article->title}}</h5>

                @foreach ($article->category as $category)
                    <a href="{{route('category.show', compact('category'))}}" class="card-text">{{$category->name}}</a>
                @endforeach
                
                <p class="card-text">{{$article->body}}</p>
                <p class="card-text">€ {{$article->price}}</p>
                {{-- <a href="{{route('articles.show',compact('article'))}}" class="btn btn-primary">Visualizza</a> --}}
                {{-- <a href="{{route('articles.edit',compact('article'))}}" class="btn btn-warning">Modifca</a>


                <form action="{{route('articles.delete',compact('article'))}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger mt-4">Elimina</button>
                </form> --}}
            </div>
        </div>
    </div>



</x-layout>