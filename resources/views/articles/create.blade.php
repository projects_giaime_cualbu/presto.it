
<x-layout>


    <div class="container mt-4 mb-4 vh-100">
        <div class="row">
            <div class="col-12">
                <h1>{{__('ui.new_article')}}</h1>
            </div>
        </div>

        <!-- Success Message -->
        @if (session('message'))
        <div class="alert alert-success py-2 shadow my-4">
            <p>{{session('message')}}</p>
        </div>
        @endif

        <!-- Error Message -->
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger shadoow my-4 py-2">
            <p>{{$error}}</p>
        </div>
        @endforeach
        @endif

        

        <form action="{{route('articles.store')}}" method="POST">
            @csrf


            <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">

            <div class="mb-3">
                <label for="title" class="form-label">{{__('ui.title')}}</label>
                <input name="title" value="{{old('title')}}" type="text" class="form-control @error('title') is-invalid @enderror " id="title" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <textarea name="body" class="form-control @error('body') is-invalid @enderror" placeholder="{{__('ui.write_message')}}" id="" cols="30" rows="10">{{old('body')}}</textarea>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">€</span>
                <input name="price" type="text" class="form-control" aria-label="Amount (to the nearest euro)">
                <span class="input-group-text">.00</span>
            </div>

            <select name="Category" class="form-select mb-3" aria-label="select example">
                <option selected>{{__('ui.choose_category')}}</option>
                
                @foreach ($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
                
            </select>

            <div class="form-group mb-3">
                <label for="images">{{__('ui.image')}}</label>
                <div>
                    <div class="dropzone" id="drophere">
                        {{-- <a href="{{route('articles.images.remove')}}">Rimuovi</a> --}}
                    
                    </div>
                    
                    @error('images')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        
                    @enderror

                </div>
            </div>

            <button type="submit" class="btn btn-primary">{{__('ui.create')}}</button>
            
        </form>

    </div>
    </div>


</x-layout>