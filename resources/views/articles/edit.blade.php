<x-layout>

    <x-slot name="title">Modifica articolo</x-slot>


    <div class="container mt-4 mb-4 vh-100">
        <div class="row">
            <div class="col-12">
                <h1>Modifica l'articolo {{$article->title}}</h1>
                
                <!-- Success Message -->
                @if (session('message'))
                <div class="alert alert-success py-2 shadow my-4">
                    <p>{{session('message')}}</p>
                </div>
                @endif
                
                <!-- Error Message -->
                @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger shadoow my-4 py-2">
                    <p>{{$error}}</p>
                </div>
                @endforeach
                @endif
                
                <form action="{{route('articles.update',compact('article', 'categories'))}}" method="POST">
                    @csrf
                    @method('PUT')
                    
                    <div class="mb-3">
                        <label for="title" class="form-label">{{__('ui.title')}}</label>
                        <input name="title" value="{{old('title')}}" type="text" class="form-control @error('title') is-invalid @enderror " id="title" aria-describedby="emailHelp">
                    </div>
                    <div class="mb-3">
                        <textarea name="body" class="form-control @error('body') is-invalid @enderror" placeholder="{{__('ui.write_message')}}" id="" cols="30" rows="10">{{old('body')}}</textarea>
                    </div>
                    
                    <div class="input-group mb-3">
                        <span class="input-group-text">€</span>
                        <input name="price" type="text" class="form-control" aria-label="Amount (to the nearest euro)">
                        <span class="input-group-text">.00</span>
                    </div>
                    
                    <select name="Category" class="form-select mb-3" aria-label="select example">
                        <option selected>{{__('ui.choose_category')}}</option>
                        
                        @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                        
                    </select>
                    
                    <button type="submit" class="btn btn-primary">{{__('ui.create')}}</button>
                    
                </form>
            </div>
        </div>
                
    </div>
            
            
            
        </x-layout>