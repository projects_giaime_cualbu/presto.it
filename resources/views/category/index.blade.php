<x-layout>

    <div class="container mt-4 mb-4">
        <div class="row">
            <div class="col-12">
                <h1>{{ __('ui.all_categories')}}</h1>
            </div>
        </div>



        <!--  Cicliamo le categorie che ci siamo presi dal DB -->
        <div class="row">
            @foreach ($categories as $category)
            <div class="col-12 col-md-4 mt-4">
                <div class="card text-white">
                    <img class="card-img-top" src="https://picsum.photos/200" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Categoria: {{$category->name}}</h5>

                        <a href="{{route('category.show', compact('category'))}}" class="btn btn-warning">Visualizza</a>
                        
                        
                    </div>
                </div>

            </div>

            @endforeach
        </div>

    </div>
    


</x-layout>