<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contatto</title>
</head>
<body>
    <h1>Ciao admin, hai ricevuto un nuovo contatto</h1>
    <ul>
        <li>Nome : {{$contact['name']}}</li>
        <li>Email : {{$contact['email']}}</li>
        <li>Messaggio : {{$contact['message']}}</li>
    </ul>
</body>
</html>