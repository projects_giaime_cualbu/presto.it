<x-layout>


                <h3 class="card-title text-center mt-5">{{ __('ui.nav_register')}}</h3>
                
                <div class="container vh-100 mt-2">
                    <div class="row">
                        <div class="col-12 mt-3">
                            <form action="{{route('register')}}" method="POST">
                                @csrf
                                <div class="mb-4">
                                    <label for="name">Nome</label>
                                    <input name="name" type="text" class="form-control form-control-sm" id="name" aria-describedby="name">
                                </div>
                                <div class="mb-4">
                                    <label for="email">Email</label>
                                    <input name="email" type="text" class="form-control form-control-sm" id="email" aria-describedby="name">
                                </div>
                                <div class="mb-4">
                                    <label for="password">Password</label>
                                    <input name="password" type="password" class="form-control form-control-sm" id="password">
                                </div>
                                <div class="mb-4">
                                    <label for="password_confirmation">Password Confirmation</label>
                                    <input name="password_confirmation" type="password" class="form-control form-control-sm" id="password_confirmation">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block mt-3">Registrati</button>

                            </form>
                        </div>
                    </div>
                </div>



</x-layout>