<x-layout>



                <h3 class="card-title text-center mt-5">Login</h3>
                
                <div class="container vh-100 mt-5">
                    <div class="row"> 
                        <div class="col-12 mt-3">  
                            <form action="{{route('login')}}" method="POST">
                                @csrf
                                
                                <div class="form-group mt-4">
                                    <label for="email">Email</label>
                                    <input name="email" class="form-control form-control-sm" id="email" aria-describedby="emailHelp">
                                </div>
                                <div class="form-group mt-4">
                                    <label for="password">Password</label>
                                    <input name="password" class="form-control form-control-sm" type="password" id="password">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block mt-4 mb-2">Login</button>

                                <div class="sign-up mt-3">
                                    Non hai un'account? <a href="{{route('register')}}">Creane uno</a>
                                </div>
                            </form>
                        </div>
                    </div>  
                </div>
            




</x-layout>