<x-layout>
    <h1 class="text-center mt-5">Ciao {{Auth::user()->name}}</h1>
    <h4 class="text-center">Qui puoi vedere i tuoi articoli, modificarli o anche eliminarli</h4>

    <div class="container mt-5 mb-5">
        <div class="row">
            @foreach (Auth::user()->articles as $article)
            <div class="col-12 col-md-4 mt-4">
                <div class="card text-white">
                    {{-- Carousel --}}
                    <div id="ad-{{$article->id}}" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            @foreach($article->images as $image)
                            <div class="carousel-item @if($loop->first) active @endif">
                                <img src="{{$image->getUrl(300, 150)}}" alt="" class="img-fluid card-img-top">
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#ad-{{$article->id}}" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#ad-{{$article->id}}" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                            @endforeach
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <h5 class="card-title text-truncate">Titolo: {{$article->title}}</h5>
                        @foreach ($article->category as $category)
                            <a href="{{route('category.show', compact('category'))}}" class="card-text">{{$category->name}}</a>
                        @endforeach
                        <p class="card-text text-truncate">{{$article->body}}</p>
                        <p class="card-text">€ {{$article->price}}</p>
                        
                        <a href="{{route('articles.show',compact('article'))}}" class="btn btn-primary ">Visualizza</a>
                        <a href="{{route('articles.edit',compact('article'))}}" class="btn btn-warning "><i class="fas fa-edit"></i> Modifica</a> 

                        <form action="{{route('articles.delete',compact('article'))}}" method="POST">
                        @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger my-2"><i class="fas fa-trash me-1"></i>Elimina</button>
                        </form> 
                        
                    </div>
                </div>

            </div>

            @endforeach
            
        </div>
    </div>








</x-layout>