
<x-layout>


    <div class="container mt-4 mb-4 vh-100">
        <div class="row">
            <div class="col-12">
                <h1>Diventa revisore</h1>
            </div>
        </div>

        <!-- Success Message -->
        @if (session('message'))
        <div class="alert alert-success py-2 shadow my-4">
            <p>{{session('message')}}</p>
        </div>
        @endif

        <!-- Error Message -->
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger shadoow my-4 py-2">
            <p>{{$error}}</p>
        </div>
        @endforeach
        @endif

        <form action="{{route('revisor.send')}}" method="POST">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nome</label>
                <input name="name" value="{{old('name')}}" type="text" class="form-control @error('name') is-invalid @enderror " id="name" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input name="email" value="{{old('email')}}" type="email" class="form-control @error('email') is-invalid @enderror " id="email" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <textarea name="message" id="" cols="50" rows="10" placeholder="Scrivi qui il tuo messaggio"></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Invia richiesta</button>
        </form>

    </div>
    </div>


</x-layout>