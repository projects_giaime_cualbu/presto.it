<x-layout>




    


    @if ($article)
        
    
    
    
    
    <div class="container">
        <div class="row ">
            <div class="col-12 mt-5">

                <div class="card-header"><h2>Annuncio # {{$article->id}}</h2></div>

                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-12 col-md-2">
                            <h3>Utente:</h3>
                        </div>
                        <div class="col-10 d-flex flex-column">
                            <p>Id: {{$article->id}}</p>
                            <p>Name: {{$article->user->name}}</p>
                            <p>Email: {{$article->user->email}}</p>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-12 col-md-2">
                            <h3>Titolo</h3>
                        </div>
                        <div class="col-10">{{$article->title}}</div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-12 col-md-4 col-lg-2">
                            <h3>Descrizione</h3>
                        </div>
                        <div class="col-8"><p>{{$article->body}}</p></div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-12 col-md-2">
                            <h3>Immagini</h3>
                        </div>
                        <div class="col-12">
                            
                            @foreach($article->images as $image)
                                
                            <div class="row justify-content-center align-items-center">
                                <div class="col-12 col-md-6 my-4">
                                    <img src="{{$image->getUrl(300, 150)}}" alt="" class="img-fluid">
                                </div>
                                <div class="col-12 col-md-3 ">
                                    <ul>
                                        <li>
                                            Adult: {{$image->adult}}

                                        </li>
                                        <li>
                                            Spoof: {{$image->spoof}}
                                        </li>
                                        <li>
                                            medical: {{$image->medical}}
                                        </li>
                                        <li>
                                            Violence: {{$image->violence}}
                                        </li>
                                        <li>
                                            Racy: {{$image->racy}}
                                        </li>

                                    </ul>
                                </div>
                                <div class="col-12 col-md-3 ">
                                    <h4>Etichette</h4>
                                    <ul class="list-unstyled">
                                        @if ($image->labels)
                                            @foreach ($image->labels as $label)
                                                
                                            <li>
                                                {{$label}}
                                            </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>


                                
                            </div>
                            @endforeach
                            
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="row mt-5 mb-5">
            <div class="col-6">
                <form action="{{route('revisor.reject', $article->id)}}" method="POST">
                @csrf
                    <button type="submit" class="btn btn-danger">Respingi</button>
                
                </form>

            </div>

            <div class="col-6 d-flex justify-content-end">
                <form action="{{route('revisor.accept', $article->id)}}" method="POST">
                @csrf
                    <button type="submit" class="btn btn-success">Accetta</button>
                
                </form>

            </div>
        </div>        



    </div>
    
    @else
        <h2 class="text-center mt-5 vh-100">Non ci sono annunci da revisionare</h2>
    
    
    
    @endif


</x-layout>