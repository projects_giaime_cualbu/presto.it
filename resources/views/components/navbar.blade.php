    <nav class="navbar navbar-expand-lg navbar-dark nav-custom">
        <div class="container-fluid">
        <a class="navbar-brand" href="/">Presto.it</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('articles.index',)}}">{{ __('ui.all_ads')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('category.index')}}">{{ __('ui.all_categories')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('articles.create')}}">{{ __('ui.nav_insert_ad')}}</a>
            </li>
            <li class="nav-item">
                @include('components.locale', ['lang' => 'it', 'nation' => 'it'])
            </li>
            <li class="nav-item">
                @include('components.locale', ['lang' => 'en', 'nation' => 'gb'])
            </li>
            <li class="nav-item">
                @include('components.locale', ['lang' => 'es', 'nation' => 'es'])
            </li>

            
            </ul>

            


            @guest
            <div class="text-end">
                <a href="{{route('login')}}" class="btn btn-outline-light me-2">Login</a>
                <a href="{{route('register')}}" class="btn btn-warning me-3">{{ __('ui.nav_register')}}</a>
            </div>

            @else

            
            @if (Auth::user()->is_revisor)

            <div class="nav-item">
                <a class="nav-link text-white" href="{{route('revisor.index')}}">Area revisore
                <span class="badge badge-pill bg-danger badge-warning">{{\App\Models\Article::ToBeRevisionedCount()}}</span>
                
                </a>

            </div>


            @else

            <div class="nav-item">
                <a class="text-decoration-none text-white me-4 " href="{{route('revisor.request')}}">Diventa revisore</a>
            </div>

            @endif

            <div class="dropdown me-5 pe-2">
                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                    {{Auth::user()->name}}
                </a>

                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <li><a class="dropdown-item" href="{{route('user.index')}}">Profilo</a></li>
                    <li><button class="dropdown-item" href="#" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">Logout</button></li>
                </ul>
                <form id="form-logout" action="{{route('logout')}}" method="POST">
                    @csrf
                </form>
            </div>


            @endguest
            {{-- Form Search --}}
            {{-- <form action="{{route('search')}}" class="d-flex" method="GET">
                <span>
                    <input name="q" class="form-control me-2" type="text" placeholder="Search" aria-label="Search">
                </span>
                <button class="btn btn-outline-success" type="submit">Ricerca</button>
            </form> --}}
        </div>
        </div>
    </nav>