<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <link href='fonts/fonts.css' rel='stylesheet'>

        
        <link rel="stylesheet" href="/css/app.css">



        <title>{{$title ?? 'Presto.it'}}</title>
    </head>

    <body>
        <x-navbar/>


            {{$slot}}
            
        
            <a id="button"><i class="fas fa-chevron-up fa-2x btn-1 text-white"></i></a>
            
            
            <x-footer/>
        

        
        <script src="/js/app.js"></script>
    </body>
</html>
