<footer class="footer d-flex flex-wrap justify-content-between align-items-center py-3 border-top">
    <div class="col-md-4   d-flex align-items-center">
      <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
        <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"></use></svg>
      </a>
      <span class="text-white">Presto.it</span>
    </div>

    <ul class="nav me-5 pe-5 col-md-4 justify-content-end list-unstyled d-flex ">
      <li class="ms-3"><a class=" text-white" href="#"><i class="fab fa-twitter fa-2x"></i></a></li>
      <li class="ms-3"><a class=" text-white"  href="#"><i class="fab fa-instagram fa-2x"></i></a></li>
      <li class="ms-3"><a class=" text-white"  href="#"><i class="fab fa-facebook fa-2x"></i></a></li>
    </ul>
  </footer>