<form action="{{route('locale', $lang)}}" method="POST">
    @csrf
    
        <button style="border: none; background-color:transparent;" class="nav-link" type="submit">
            <span class="flag-icon flag-icon-{{$nation}}"></span>
        </button>

</form>