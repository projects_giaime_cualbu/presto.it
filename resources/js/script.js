$(function(){

    // document ready

    if($("#drophere").length > 0 ){
    
        let csrfToken = $('meta[name="csrf-token"]').attr('content');
        let uniqueSecret = $('input[name="uniqueSecret"]').attr('value');

        let myDropzone = new Dropzone('#drophere', {
            url: '/immagini/articolo/upload',

            params: {
                _token: csrfToken,
                uniqueSecret: uniqueSecret
            },

            addRemoveLinks: true,

            init: function(){
                $.ajax({
                    type: 'GET',
                    url: '/immagini/articolo',
                    data: {
                        uniqueSecret: uniqueSecret
                    },
                    dataType: 'json'
                }).done(function(data){
                    $.each(data, function(key, value){
                        let file = {
                            serverId: value.id
                        };

                        myDropzone.options.addedfile.call(myDropzone, file);
                        myDropzone.options.thumbnail.call(myDropzone, file, value.src);
                    });
                });
            }
        });
        myDropzone.on("success", function(file, response){
            file.serverId = response.id;
        });
        myDropzone.on("removedfile", function(file){
            $.ajax({
                type: 'DELETE',
                url: '/rimuovi/immagini/articolo',
                data: {
                    _token: csrfToken,
                    id:file.serverId,
                    uniqueSecret: uniqueSecret
    
    
                },
                dataType: 'json'
    
    
            });
        });
    }













});

//BOTTONE BACK TO TOP

var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 200) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});


