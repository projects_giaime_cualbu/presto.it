<?php

return [

    'welcome' => 'Bienvenido in Presto.it',
    'all_ads' => 'Todos los anuncios',
    'all_categories' => 'Todas las categorias',
    'nav_insert_ad' => 'Publicar anuncio',
    'nav_register' => 'Registrarse',
    'insert_button' => 'Publicar anuncio',
    'search_title' => 'Buscar algo',
    'search_button' => 'Buscar',
    'last_ads' => 'Últimos 6 anuncios',
    'new_article' => 'Nuevo artículo',
    'title' => 'Título',
    'write_message' => 'Escribe el mensaje',
    'choose_category' => 'Elige la categoría',
    'image' => 'Imagenes',
    'create' => 'Crea'



];