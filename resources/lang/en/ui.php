<?php

return [

    'welcome' => 'Welcome to Presto.it',
    'all_ads' => 'All ads',
    'all_categories' => 'All categories',
    'nav_insert_ad' => 'Post ad',
    'nav_register' => 'Register',
    'insert_button' => 'Post ad',
    'search_title' => 'Search something',
    'search_button' => 'Search',
    'last_ads' => '6 recent ads',
    'new_article' => 'New Article',
    'title' => 'Title',
    'write_message' => 'Write the message',
    'choose_category' => 'Choose the category',
    'image' => 'Image',
    'create' => 'Create'

    

];