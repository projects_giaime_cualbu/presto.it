<?php

return [

    'welcome' => 'Benvenuto su Presto.it',
    'all_ads' => 'Tutti gli annunci',
    'all_categories' => 'Tutte le categorie',
    'nav_insert_ad' => 'Inserisci annuncio',
    'nav_register' => 'Registrati',
    'insert_button' => 'Inserisci annuncio',
    'search_title' => 'Cerca qualcosa',
    'search_button' => 'Ricerca',
    'last_ads' => 'Ultimi 6 Annunci',
    'new_article' => 'Nuovo articolo',
    'title' => 'Titolo',
    'write_message' => 'Scrivi il messaggio',
    'choose_category' => 'Scegli la categoria',
    'image' => 'Immagini',
    'create' => 'Crea',


];