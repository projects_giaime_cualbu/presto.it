<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoriesController;

use App\Http\Controllers\MailController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index']);


// Gestire l'entità ARTICLE

//ROTTE PER LA CREATE
//Create e Store

Route::get('/crea/articolo', [ArticleController::class, 'create'])->name('articles.create');
Route::post('/salva/articolo', [ArticleController::class, 'store'])->name('articles.store');

Route::post('/immagini/articolo/upload', [ArticleController::class, 'uploadImage'])->name('articles.images.upload');

Route::delete('/rimuovi/immagini/articolo', [ArticleController::class, 'removeImage'])->name('articles.images.remove');

Route::get('/immagini/articolo', [ArticleController::class, 'getImages'])->name('articles.images');

//ROTTE PER LA READ
//Index e Show

Route::get('/tutti/gli/articoli', [ArticleController::class, 'index'])->name('articles.index');
// ROUTE MODEL BINDING
Route::get('/dettaglio/articolo/{article}', [ArticleController::class, 'show'])->name('articles.show');


//ROTTE PER LA UPDATE
Route::get('/modifica/articolo/{article}', [ArticleController::class, 'edit'])->name('articles.edit');
Route::put('/aggiorna/articolo/{article}', [ArticleController::class, 'update'])->name('articles.update');

//ROTTE PER DELETE
Route::delete('/elimina/articolo/{article}', [ArticleController::class, 'delete'])->name('articles.delete');

//ROTTE PER CATEGORIE

Route::get('/tutte/le/categorie', [CategoriesController::class, 'index'])->name('category.index');

Route::get('/articoli/della/categoria/{category}', [CategoriesController::class, 'show'])->name('category.show');

//ROTTE REVISORE
Route::get('/revisore/home',[RevisorController::class,'index'])->name('revisor.index'); 

Route::post('/revisore/articolo/{id}/accept',[RevisorController::class,'accept'])->name('revisor.accept');    

Route::post('/revisore/articolo/{id}/reject',[RevisorController::class,'reject'])->name('revisor.reject'); 

// ROTTE PER LA SEARCH

Route::get('/ricerca', [PublicController::class, 'search'])->name('search.results');


// ROTTA INVIO MAIL
Route::get('/diventa/revisore', [MailController::class, 'request'])->name('revisor.request');

Route::post('/invio/contatto', [MailController::class, 'sendRequest'])->name('revisor.send');

//ROTTE CAMBIO LINGUA

Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');

//ROTTA PAGINA PROFILO

Route::get('/mio/profilo', [UserController::class, 'index'])->name('user.index');