<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Article extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['title', 'body', 'price', 'user_id'];

    //Search Scout

    public function toSearchableArray()
    {
        $category = $this->category->pluck('name');
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'altro' => 'auto macchina calcio baseball tennis sesso amore precauzioni',
            'category' => $category
            ];

        

        return $array;
    }

    //Many to many con categorie

    public function Category()
    {
        return $this->belongsToMany(Category::class);
    }

    // One to many con user

    public function user(){
        return $this->belongsTo(User::class);
    }

    //  METODO CONTATORE

    static public function ToBeRevisionedCount()
    {
        return Article::where('is_accepted', null)->count();
    }
    

    //RELAZIONE FRA ARTICOLO ED IMMAGINI

    public function images(){

        return $this->hasMany(ArticleImage::class);
    }
}
