<?php

namespace App\Http\Controllers;

use App\Mail\RevisorMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function request(){
        return view('revisor.request');
    }

    public function sendRequest(Request $request){
        $contact = $request->all();

        Mail::to('emailAdmin@gmail.com')->send(new RevisorMail($contact));

        return redirect()->back()->with('message','Richiesta inviata');
    }
}
