<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index()
    {
        $articles = Article::where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->take(6)
        ->get();

        return view('welcome', compact('articles'));
    }

    public function search(Request $request)
    {
        $q = $request->input('q');

        

        $articles = Article::search($q)->where('is_accepted', true)->get();

        

        return view('search.results', compact('q', 'articles'));
    }

    public function locale($locale)
    {
        session()->put('locale', $locale);
        return redirect()->back();
    }

    
}
