<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\ArticleImage;
use Illuminate\Http\Request;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Requests\ContactRequest;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'store']);
    }

    public function create(Request $request)
    {
        $categories = Category::all();

        $uniqueSecret = $request->old(
            'uniqueSecret', 
            base_convert(sha1(uniqid(mt_rand())), 16, 36));

        return view('articles.create', compact('categories', 'uniqueSecret'));
    }
    public function store(ContactRequest $request)
    {
        $choosenCategories = $request->Category;

        $article = Article::create([
            'title' =>$request->title,
            'body' =>$request->body,
            'price' =>$request->price,
            'user_id' =>Auth::id()
        ]);
        
        $article->Category()->attach($choosenCategories);

        $uniqueSecret = $request->input('uniqueSecret');
        
        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        foreach($images as $image) {

            $i = new ArticleImage();

            $fileName = basename($image);
            $newFileName = "public/articles/{$article->id}/{$fileName}";

            Storage::move($image, $newFileName);


            $i->file = $newFileName;
            $i->article_id = $article->id;

            $i->save();


            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                new ResizeImage($i->file,300,150)
            ])->dispatch($i->id);

        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));

        return redirect()->back()->with('message', 'Complimenti! Articolo pubblicato');
    }

    //Metodi READ
    public function index()
    {
        $articles = Article::where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->take(10)
        ->get();
        return view('articles.index', compact('articles'));
    }

    public function show(Article $article)
    {
        return view('articles.show', compact('article'));
    }

    //metodo per l'update

    public function edit(Article $article)
    {
        $categories = Category::all();

        return view('articles.edit', compact('article', 'categories'));
    }

    public function update(Article $article, ContactRequest $request)
    {
        $article->update($request->all());
        return redirect()->back()->with('message', 'Complimenti! Articolo modificato');
    }

    //Metodo per la delete

    public function delete(Article $article)
    {
        $article->delete();
        
        return redirect(route('articles.index'))->with('message', 'Articolo eliminato correttamente!');
    }

    public function uploadImage(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName,
            120,
            120
        ));

        session()->push("images.{$uniqueSecret}", $fileName);

        return response()->json([

            'id'=> $fileName,
        ]);
    }


    public function removeImage(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);

        return response()->json('ok');
    }

    public function getImages(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data = [];

        foreach($images as $image){

            
            $data[] = [
                'id' => $image,
                'src' => ArticleImage::getUrlByFilePath($image, 120, 120)

            ];
        }

        return response()->json($data);
    }
}
