<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:40',
            'body' => 'required|min:20',
            'price' => 'required',
            'Category' => 'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Il titolo è obbligatorio',
            'title.min' => 'Il titolo deve avere minimo 3 caratteri',
            'title.max' => 'Il titolo può avere massimo 20 caratteri',
            'body.required' => 'Il testo è obbligatorio',
            'body.min' => 'Il testo deve avere minimo 20 caratteri',
            'Category.numeric' => 'Devi scegliere una categoria',
            'price.required' => 'Devi inserire un prezzo'

        ];
    }
}
